# 1️⃣ Criando um Deploy de uma aplicação
Neste projeto será criado um pipeline de deploy de uma aplicação com cenários de produção de imagens com Docker e criação dos deployments em um cluster kubernetes em nuvem utilizando o GCP.

## Código fonte da aplicação
https://github.com/denilsonbonatti/k8s-projeto1-app-base

## Descrição
- A aplicação rodará em GCP com o uso de clusters e load balancer
- Toda a aplicação está na pasta app